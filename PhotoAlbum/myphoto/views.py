from django.shortcuts import render
from django.http import HttpResponse
from myphoto.models import Pictures
from django.core.paginator import Paginator
from datetime import datetime
from PIL import Image
import time,os

# Create your views here.

def index(request):
	return render(request,'myphoto/index.html')

def browsePhoto(request,pIndex):
	'''浏览相册信息'''
	list1 = Pictures.objects.all() #获取用户信息
	p = Paginator(list1,5) #分页操作
	if pIndex == '':
		pIndex = '1'
	list2 = p.page(pIndex)
	plist = p.page_range
	context = {'plist':list2,"page":plist,"pIndex":int(pIndex)}#将用户信息、分页信息封装到字典中
	return render(request,'myphoto/browsephoto.html',context)

def editPhoto(request,pid):
	# return HttpResponse("id"+pid)
	ob = Pictures.objects.get(id=pid)
	context = {'pinfo':ob}
	return render(request,'myphoto/editphoto.html',context)

def addPhoto(request):
	return render(request,'myphoto/addphoto.html')

def insertPhoto(request):
	try:
		'''执行图片上传'''
		myfile = request.FILES.get("pic",None)
		if not myfile :
			return HttpResponse("没有上传的文件！")
		filename = str(time.time())+'.'+myfile.name.split('.').pop()
		destination = open("./static/pics/"+filename,"wb+")
		for chunk in myfile.chunks():     #分块写入文件
			destination.write(chunk)
		destination.close()

		'''执行图片缩放'''
		im = Image.open("./static/pics/"+filename)
		# 缩放到75*75(缩放后的宽高比例不变):
		im.thumbnail((75,75))
		# 把缩放后的图像用jpeg格式保存:
		im.save("./static/pics/s_"+filename,None)
		#执行图片删除
		#os.remove("./static/pics/"+filename)
		# print(myfile)
		# return HttpResponse("上传成功！图片："+filename)

		'''添加相册'''
		p = Pictures()
		p.title = request.POST['title']
		# p.picture = request.POST['picture']
		p.picture = 's_'+filename
		p.time = datetime.now()
		p.save()
		context = {'info':"添加成功！"}
	except Exception as err:
		print(err)
		context = {'info':'添加失败！'}
	return render(request,'myphoto/info.html',context)

def delPhoto(request,pid):
	try:
		ob = Pictures.objects.get(id=pid)
		'''删除图片'''
		filename = ob.picture
		s_filename = filename.strip('s_')
		os.remove("./static/pics/"+filename)
		os.remove("./static/pics/"+s_filename)
		ob.delete()
		context = {"info":"删除成功！"}
	except:
		context = {"info":"删除失败！"}
	return render(request,'myphoto/info.html',context)

def updatePhoto(request):
	try:
		'''执行图片上传'''
		myfile = request.FILES.get("pic",None)
		#判断是否上传有图片
		if not myfile :
			try:
				p = Pictures.objects.get(id=request.POST['id'])
				p.title = request.POST['title']
				p.save()
				context = {'info':'修改成功！'}
			except:
				context = {'info':'修改失败！'}
			return render(request,'myphoto/info.html',context)
		filename = str(time.time())+'.'+myfile.name.split('.').pop()
		destination = open("./static/pics/"+filename,"wb+")
		for chunk in myfile.chunks():     #分块写入文件
			destination.write(chunk)
		destination.close()

		# 执行图片缩放
		im = Image.open("./static/pics/"+filename)
		# 缩放到75*75(缩放后的宽高比例不变):
		im.thumbnail((75,75))
		# 把缩放后的图像用jpeg格式保存:
		im.save("./static/pics/s_"+filename,None)
		#执行图片删除
		oldfile = request.POST.get("oldpic",None)
		s_oldfile = oldfile.strip('s_')
		os.remove("./static/pics/"+oldfile)
		os.remove("./static/pics/"+s_oldfile)
		print("删除旧图成功！")

		'''修改相册'''
		p = Pictures.objects.get(id=request.POST['id'])
		p.title = request.POST['title']
		p.picture = 's_'+filename
		p.save()
		context = {"info":"修改成功"}
	except Exception as erro:
		print(erro)
		context = {"info":"修改失败！"}
	return render(request,'myphoto/info.html',context)


