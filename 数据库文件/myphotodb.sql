/*
SQLyog 企业版 - MySQL GUI v8.14 
MySQL - 5.5.5-10.1.34-MariaDB : Database - myphotodb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`myphotodb` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `myphotodb`;

/*Table structure for table `django_migrations` */

DROP TABLE IF EXISTS `django_migrations`;

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `django_migrations` */

/*Table structure for table `pictures` */

DROP TABLE IF EXISTS `pictures`;

CREATE TABLE `pictures` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` char(32) DEFAULT NULL,
  `picture` char(32) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

/*Data for the table `pictures` */

insert  into `pictures`(`id`,`title`,`picture`,`time`) values (2,'乔治','s_1542076277.8316572.png','2018-11-12 09:48:50'),(3,'亚当斯','s_1542076419.3243442.png','2018-11-12 09:49:10'),(4,'格兰特','s_1542076432.1298819.png','2018-11-12 09:49:26'),(5,'罗伯森','s_1542076448.5973194.png','2018-11-12 09:49:47'),(6,'弗格森','s_1542076477.0708609.png','2018-11-12 09:49:59'),(7,'斯罗德','s_1542076493.138975.png','2018-11-12 09:50:38'),(8,'阿布','s_1542076506.7617378.png','2018-11-12 09:51:10'),(9,'迪亚洛','s_1542076526.6974573.png','2018-11-12 09:51:58'),(11,'伊巴卡','s_1542076542.0875256.png','2018-11-12 14:04:55'),(12,'奥拉迪波1','s_1542076568.2018893.png','2018-11-12 14:12:40'),(16,'雷管','s_1542076602.5323527.jpg','2018-11-13 07:49:12'),(18,'威少','s_1542087731.8856993.png','2018-11-13 12:11:11');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
