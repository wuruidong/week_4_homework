from django.db import models
from datetime import datetime

# Create your models here.

class Pictures(models.Model):
	title = models.CharField(max_length=32)
	# title = models.CharField(max_length=20)
	picture = models.CharField(max_length=32)
	time = models.DateTimeField(default=datetime.now)

	class Meta:
		db_table = 'pictures' #指定真实表名
