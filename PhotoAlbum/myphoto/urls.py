from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index,name='index'),
    url(r'^browsephoto/(?P<pIndex>[0-9]+)$', views.browsePhoto,name='browsephoto'),
    url(r'^addphoto$', views.addPhoto,name='addphoto'),
    url(r'^insertphoto$', views.insertPhoto,name='insertphoto'),
    url(r'^editphoto/(?P<pid>[0-9]+)$', views.editPhoto,name='editphoto'),
    url(r'^delphoto/(?P<pid>[0-9]+)$', views.delPhoto,name='delphoto'),
    url(r'^updatephoto$', views.updatePhoto,name='updatephoto'),
]